#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
#from matplotlib.figure import Figure
import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
import os

import sys
sys.path.insert(1, '/home/owner/codes/finance')
import finances_func
from income_plot import inc_plot
from expense_plot import exp_plot
from transaction import transaction


LARGE_FONT= ("Verdana", 12)


class chedr(tk.Tk):

    def __init__(self, *args, **kwargs):
      
        tk.Tk.__init__(self, *args, **kwargs)
        #tk.Tk.iconbitmap(self, default="clienticon.ico")
        tk.Tk.wm_title(self, "cHEDr")
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand = True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (StartPage, dataEntry, PageTwo, PageThree):

            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):

        frame = self.frames[cont]
        frame.tkraise()

        
class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self, text="Welcome", font=LARGE_FONT)
        label.pack(pady=10,padx=10)

        button = ttk.Button(self, text="Import Data",
                            command=lambda: controller.show_frame(dataEntry))
        button.pack()

        button2 = ttk.Button(self, text="Visit Page 2",
                            command=lambda: controller.show_frame(PageTwo))
        button2.pack()

        button3 = ttk.Button(self, text="Graph Page",
                             command=lambda: controller.show_frame(PageThree))
        button3.pack()


class dataEntry(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Import Data", font=LARGE_FONT)
        label.pack(pady=10,padx=10)
        label.place(x=0,y=0)
        # Downloads query
        label = tk.Label(self, text="Downloads Folder:", font=LARGE_FONT)
        label.pack(pady=10,padx=10)
        label.place(x=0,y=50)
        dir_downloads = '/home/owner/Downloads/'
        downloads_enter = ttk.Entry(self,width=30)
        downloads_enter.insert(0,dir_downloads)
        downloads_enter.pack()
        downloads_enter.place(x=0,y=75)
        # Finances query
        lbl_f = tk.Label(self, text="Data Folder:", font=LARGE_FONT)
        lbl_f.pack(pady=10,padx=10)
        lbl_f.place(x=0,y=100)
        dir_finances = '/home/owner/data/finances/'
        data_dir_enter = ttk.Entry(self,width=30)
        data_dir_enter.insert(0,dir_finances)
        data_dir_enter.pack()
        data_dir_enter.place(x=0,y=125)
        # Open data button
        def opendatas():
            down_string = downloads_enter.get()
            data_string = data_dir_enter.get()
            if not os.path.exists(down_string): # Checking downloads folder
                label = tk.Label(self, text="Downloads directory incorrect!", font=LARGE_FONT)
                label.pack(pady=10,padx=10)
                label.place(x=225,y=75)
            elif not os.path.exists(data_string): # Checking data folder
                label = tk.Label(self, text="Data directory incorrect!", font=LARGE_FONT)
                label.pack(pady=10,padx=10)
                label.place(x=225,y=125)
            else: # Getting the files and displaying plot
                files = finances_func.getfilesnew(down_string,data_string)
                csv_data = transaction.read_files(files)
                button2 = ttk.Button(self, text="cont.",command=lambda: controller.show_frame(PageTwo))
                button2.pack()
                button2.place(x=0,y=175)
                
                
                starting = 1903.15 # Oct 1, 2019
                im = transaction.monthlychanges(csv_data,starting)
                canvas = FigureCanvasTkAgg(im,self)#, master=dataEntry(tk.Frame))
                canvas.draw()
                canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=None, expand=False)
                toolbar = NavigationToolbar2Tk(canvas, self)
                toolbar.update()
                canvas._tkcanvas.pack(side=tk.TOP, fill=None, expand=None)
                button2 = ttk.Button(self, text="cont.",command=lambda: controller.show_frame(PageTwo))
                button2.pack()
                button2.place(x=0,y=175)
#                canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
#                toolbar = NavigationToolbar2Tk(canvas, root)
#                toolbar.update()
#                canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
                #load = Image.open("/home/owner/data/finances/2019-incomes-expenses.png")
                #im = im.resize((350,200), Image.ANTIALIAS)
                #render = ImageTk.PhotoImage(im)
                

            # labels can be text or images
#            img = ttk.Label(self, image=render)
#            img.image = render
#            img.place(x=225, y=20)
            
            

        opendata = ttk.Button(self, text="Get data", command=opendatas)
        opendata.pack()
        opendata.place(x=0,y=150)

        # Go Home Button
        gohome = ttk.Button(self, text="Back to Home",
                            command=lambda: controller.show_frame(StartPage))
        gohome.pack()
        gohome.place(x=00, y=300)



class PageTwo(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Page Two!!!", font=LARGE_FONT)
        label.pack(pady=10,padx=10)

        button1 = ttk.Button(self, text="Back to Home2",
                            command=lambda: controller.show_frame(StartPage))
        button1.pack()

        button2 = ttk.Button(self, text="Page One",
                            command=lambda: controller.show_frame(dataEntry))
        button2.pack()


class PageThree(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Graph Page!", font=LARGE_FONT)
        label.pack(pady=10,padx=10)

        button1 = ttk.Button(self, text="Back to Home3",
                            command=lambda: controller.show_frame(StartPage))
        button1.pack()

        f = Figure(figsize=(3,2), dpi=100)
        a = f.add_subplot(111)
        a.plot([1,2,3,4,5,6,7,8],[5,6,1,3,8,9,3,5])

        

        canvas = FigureCanvasTkAgg(f, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=False)

        toolbar = NavigationToolbar2Tk(canvas, self)
        toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=False)
        
        
app = chedr()
#app.geometry('600x350')
app.geometry('1000x1000')

app.mainloop()