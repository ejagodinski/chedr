#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 16:31:52 2020

@author: owner
"""
#import matplotlib
##from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
#from matplotlib.figure import Figure
#matplotlib.use("TkAgg")

from tkinter import *
from tkinter import Label,Entry,Button,Tk
from PIL import ImageTk, Image
import random

import sys
sys.path.insert(1, '/home/owner/codes/finance')
import finances_func as F
from income_plot import inc_plot
from expense_plot import exp_plot
from transaction import transaction
#%% Get downloaded ones and get list of files
dir_downloads = '/home/owner/Downloads/'
dir_finances = '/home/owner/data/finances/'
files = F.getfilesnew(dir_downloads,dir_finances)
csv_data = transaction.read_files(files)
#%% Incomes and Expenses
months, incomes,expenses = transaction.incexp(csv_data)
#%% Plot any string over a year
transaction.plot_string_year(csv_data,'FPL','2019')
#%%

w = Tk()

# w options
w.title("Chedr")
w.geometry('800x400') # length x width
#img = ImageTk.PhotoImage(Image.open("blackmoon.jpg"))  
##background_image=PhotoImage("/home/owner/codes/blackmoon.jpg")
#background_label = Label(w, image=img)
#background_label.place(x=0, y=0, relwidth=1, relheight=1)

# Label 1
lbl1 = Label(w, text="Enter a string", font=("Arial Bold", 36))
lbl1.grid(column=0, row=0)
# Text Box
string_enter = Entry(w,width=20)
string_enter.grid(column=0, row=1)
# Label 2
lbl2 = Label(w, text="Enter a year", font=("Arial Bold", 36))
lbl2.grid(column=0, row=2)
# Text Box
year_enter = Entry(w,width=20)
year_enter.grid(column=0, row=3)

# Button 1
# function
def clicked():
    string_in = string_enter.get()
    year_in = year_enter.get()
    transaction.plot_string_year(csv_data,string_in,year_in)
  
#location and direction  
btn = Button(w, text="Open it!", command=clicked, bg="orange", fg="red")
btn.grid(column=0, row=4)

w.mainloop()