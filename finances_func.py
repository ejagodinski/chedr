#!/usr/bin/env python3
import csv
import os
import glob
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import numpy as np
import shutil

#%% Get downloaded ones
def getfilesnew(dir_downloads,dir_finances):
    os.chdir(dir_downloads)
    recents = sorted(glob.glob('Checking*'))
    for file in recents:
        with open(file) as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=',')
            for row in csv_reader:
                m = row[0].split('/')[0]
                y = row[0].split('/')[2]
        new_name = dir_finances+y+'-'+m+'.csv'
        old_name = dir_downloads+file
        shutil.copyfile(old_name,new_name)
    os.chdir(dir_finances)
    files = sorted(glob.glob('*.csv'))
    return files


#%% Getting the months and years
def yrmo_dates(dir_finances,files):
    os.chdir(dir_finances)
    mo = []
    yr = []
    mo_dates = []
    for file in files:
        with open(file) as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=',')
            for row in csv_reader:
                # dates
                m = row[0].split('/')[0]
                y = row[0].split('/')[2]
                if y not in yr:
                    #print(y)
                    yr.append(y)
                if m not in mo:
                    #print(m)
                    mo.append(m)
        mo_dates.append(m+'_'+y)
    return mo_dates,mo

#%% Fetch expenses and income
def incexp(mo,mo_dates,files):
    incomes = []
    expenses = []
    # Creating variables                
    for m in mo:
        locals()[m+'e'] = 0
        locals()[m+'i'] = 0
    # Add income and expenses
    for file in files:
        with open(file) as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=',')
            for row in csv_reader:
                #Transaction
                m = row[0].split('/')[0]
                transact = float(row[1])
                # Income vs Expense
                if transact > 0:
                    locals()[m+'i'] = locals()[m+'i'] + transact
                else:
                    locals()[m+'e'] = locals()[m+'e']+ transact
            # Rounding the monthly expenses
            locals()[m+'i'] = round(locals()[m+'i'],2)
            locals()[m+'e'] = round(locals()[m+'e'],2)
            incomes.append(locals()[m+'i'])
            expenses.append(locals()[m+'e'])
    return incomes,expenses

#%% Starting and Ending balances

def balance(starting,incomes,expenses):
    balances = [starting]
    for i in range(0,len(incomes)):
        balances.append(round(incomes[i]+expenses[i]+balances[i],2))
    return balances

#%% d_balance/d_month
def endbalpct(incomes,expenses,balances):
    pct = []
    for i in range(len(incomes)):
        diff =round(incomes[i]+expenses[i],2)
        pct.append(round(balances[i]/diff))
    return pct

#%% Make graphs of data
# List to array
def monthlychanges(incs,exps,pct,bals,modat,start,end):
    incs,exps,pct,bals,modat = incs[start:end], exps[start:end], pct[start:end], bals[start:end], modat[start:end]
    y1 = abs(np.array(incs))
    y2 = abs(np.array(exps))
    y2_bot = bals - y2
    # Plot
    im = plt.figure(num=None, figsize=(12, 5))
    # Balance changes
    for i in range(0,len(incs)):
        if pct[i]>0:
            text = '+'+str(pct[i])+'%'
            txt_col = 'g'
        else:
            text = str(pct[i])+'%'
            txt_col = 'r'
        plt.text(i-0.2,bals[i]+y1[i]+100,text,fontsize=10,color=txt_col,fontweight='bold')
    # Expenses
    #fig = Figure(figsize=(5, 4), dpi=100)
    im.add_subplot(1,1,1).bar(modat,y2,bottom = y2_bot,color='r')
    # Income
    im.add_subplot(1,1,1).bar(modat,y1,bottom=bals,color='g')
    # Plot visuals
    #end = bals[-1]+incs[-1]+exps[-1]
    #plt.plot([-0.5,len(y1)-0.5],[bals[0],end],'k',linewidth=5)
    #print('Started with $'+str(bals[0])+' and ended with $'+str(end))
    im.ylabel('Balance')
    #plt.hlines(0,-0.5,len(y1)-0.5,linestyles='dashed')
    im.title('Income and Expenses\n'+'Started with \$'+str(bals[0])+' and ended with \$'+str(round(end,2)))
    im.ylim(-4000,8000)    
    #plt.show()
    #im.savefig('2019-incomes-expenses.jpg')
    return im



