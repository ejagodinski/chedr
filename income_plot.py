#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import csv
import matplotlib.pyplot as plt
import numpy as np
def inc_plot(mo_dates,files):          
    paychecks = []
    from_parents = []
    venmos = []
    gfws = []
    invoices = []
    gifts = []
    savings = []
    others = []
    #from_parents = []
    for file in files:
        # Income categories
        paycheck = 0
        from_parent = 0
        venmo = 0
        gfw = 0
        invoice = 0
        gift = 0
        saving = 0
        other = 0
        with open(file) as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=',')
            for row in csv_reader:
                #Transaction
                #ddate = row[0]
                transact = float(row[1])
                descript = row[4]
                if transact > 0:
                    #print(descript)
                    if "PAYROLL" in descript:
                        paycheck = paycheck + transact
                    elif "FLORIDA ATLANTIC INVOICE" in descript:
                        invoice = invoice + transact                        
                    elif "FROM JAGODINSKI J" in descript:
                        from_parent = from_parent + transact
                    elif "FROM Jagodinski Eric NY" in descript:
                        venmo = venmo + transact
                        #print("")
                        #print(descript)
                    elif "GO FAR REWARDS" in descript:
                        gfw = gfw + transact                        
                    else:
                        other = other + transact                        
#                        print(date)
#                        print(transact)
#                        print(descript)
#                        print('')
#                        ins = input()
#                        if "gift" or "Gift" or "GIFT" in ins:
#                            gift = gift + transact
#                        elif "saving" or "Saving" or "savings" or "Savings" in ins:
#                            saving = saving + transact
#                        else:
#                            other = other + transact
        # Monthly totals
        paychecks.append(paycheck)
        invoices.append(invoice)
        from_parents.append(from_parent)
        venmos.append(venmo)
        gfws.append(gfw)
        gifts.append(gift)
        savings.append(saving)
        others.append(other)
    # Averages
    av_fp = round(np.mean(np.array(from_parents)),2)
    av_vnm = round(np.mean(np.array(venmos)),2)

    # Plotting
    im = plt.figure(num=None, figsize=(12, 5))
    ind = np.arange(len(mo_dates))
    plt.bar(mo_dates, paychecks, 0.7)
    bot = paychecks
    plt.bar(ind, from_parents, 0.7, bottom=bot)
    bot = np.array(bot) + np.array(from_parents)
    plt.bar(ind, venmos, 0.7, bottom=bot)
    bot = np.array(bot) + np.array(venmos)
    plt.bar(ind, others, 0.7, bottom=bot)
    bot = np.array(bot) + np.array(others)    
    plt.ylim(0,7000)
    plt.legend(['pay_checks','from_parents','venmo','other'])
    for i in range(0,len(mo_dates)):
        num = int(round(bot[i]))
        text = '+$'+str(num)
        plt.text(i-0.4,bot[i]+100,text,fontsize=10)
    plt.title('Average from parents: \$'+str(av_fp)+'  Average from venmo: \$'+str(av_vnm))
    plt.show()
    im.savefig('incomes2019.jpg')
    return paychecks,from_parents