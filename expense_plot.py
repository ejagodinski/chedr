#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import csv
import matplotlib.pyplot as plt
import numpy as np

def exp_plot(mo_dates,files): 
    categories = ['utilities','way2saves','foods','rents','gyms','transports',
                  'digitals','healths','ubers','debts','purchases','homes','others']
    category = []
    for cat in categories:
        locals()[cat] = []
        category.append(cat.split('s')[0])
    #from_parents = []
    for file in files:
        # Income categories
        for cat in categories:
            locals()[cat.split('s')[0]] = 0
        with open(file) as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=',')
            for row in csv_reader:
                #Transaction
                ddate = row[0]
                transact = float(row[1])
                descript = row[4]
                if transact < 0:
                    if "FPL" in descript:
                        locals()[category[0]] = locals()[category[0]] + transact  
                    elif "GOOGLE" in descript: # utilities
                        locals()[category[0]] = locals()[category[0]] + transact
                    elif "COMCAST" in descript: # utilities
                        locals()[category[0]] = locals()[category[0]] + transact                      
                    elif "SAVE AS YOU GO TRANSFER" in descript: # Way2Save
                        locals()[category[1]] = locals()[category[1]] + transact
                    elif "PUBLIX" in descript: # foods
                        locals()[category[2]] = locals()[category[2]] + transact
                    elif "PENN DUTCH" in descript: # foods
                        locals()[category[2]] = locals()[category[2]] + transact                    
                    elif "COSTCO" in descript: # foods
                        locals()[category[2]] = locals()[category[2]] + transact
                    elif "Jack Yunis" in descript: # rents
                        print(ddate+'\n'+str(transact)+'\n'+descript+'\n ')
                        locals()[category[3]] = locals()[category[3]] + transact 
                    elif "PLANET FIT CLUB" in descript: # gyms
                        locals()[category[4]] = locals()[category[4]] + transact
                    elif "SUNPASS" in descript: # transports
                        locals()[category[5]] = locals()[category[5]] + transact
                    elif "PATREON" in descript: # digitals 
                        locals()[category[6]] = locals()[category[6]] + transact
                    elif "Prime Video" in descript: # digitals
                        locals()[category[6]] = locals()[category[6]] + transact
                    elif "PREMIUM" in descript: # healths
                        locals()[category[7]] = locals()[category[7]] + transact                       
                    elif "UBER" in descript: # ubers
                        locals()[category[8]] = locals()[category[8]] + transact
                    elif "PLATINUM CARD" in descript: # debts
                        locals()[category[9]] = locals()[category[9]] + transact
                    elif "WF Credit Card AUTO PAY" in descript: # debts
                        locals()[category[9]] = locals()[category[9]] + transact                    
                    elif "AMZN" in descript: # purchases
                        locals()[category[10]] = locals()[category[10]] + transact
                    elif "WALGREENS" in descript: # homes
                        locals()[category[11]] = locals()[category[11]] + transact
                    elif "CVS" in descript: # homes
                        locals()[category[11]] = locals()[category[11]] + transact
                    elif "Wal-Mart" in descript:
                        if transact > 30: # foods
                            locals()[category[2]] = locals()[category[2]] + transact
                        else: # homes
                            locals()[category[11]] = locals()[category[11]] + transact 
                    elif "Microsoft" in descript:
                        if transact > 60: # utilities
                            locals()[category[1]] = locals()[category[1]] + transact
                        else: # purchases
                            locals()[category[10]] = locals()[category[10]] + transact                          
                    else:
                        locals()[category[12]] = locals()[category[12]] + transact
    
    #                    other = other + transact                        
    #                        print(date)
    #                        print(transact)
    #                        print(descript)
    #                        print('')
    #                        ins = input()
    #                        if "gift" or "Gift" or "GIFT" in ins:
    #                            gift = gift + transact
    #                        elif "saving" or "Saving" or "savings" or "Savings" in ins:
    #                            saving = saving + transact
    #                        else:
    #                            other = other + transact
        # Monthly totals
        for cat in categories:
            locals()[cat].append(round(locals()[cat.split('s')[0]],2))
    
    # Averages
    #av_fp = round(np.mean(np.array(from_parents)),2)
    avgs = []
    for cat in categories:
        cats = categories.copy()
        y = np.array(locals()[cat])
        avg = np.mean(y)
        avgs.append(avg)
    avgs,cats = (np.asarray(t) for t in zip(*sorted(zip(avgs,cats))))
    
    # Plotting
    im = plt.figure(num=None, figsize=(14, 7))
    ind = np.arange(len(mo_dates))
    bot = np.zeros((len(mo_dates)))
    for cat in cats:
        y = abs(np.array(locals()[cat]))
        plt.bar(mo_dates,y,0.7,bottom=bot)
        bot = bot + abs(y)
    plt.legend(cats)
    im.savefig('expenses2019.jpg')  
        
    #plt.bar(mo_dates, paychecks, 0.7)
    #bot = paychecks
    #plt.bar(ind, from_parents, 0.7, bottom=bot)
    #bot = np.array(bot) + np.array(from_parents)
    #plt.bar(ind, venmos, 0.7, bottom=bot)
    #bot = np.array(bot) + np.array(venmos)
    #plt.bar(ind, others, 0.7, bottom=bot)
    #bot = np.array(bot) + np.array(others)    
    #plt.ylim(0,7000)
    #plt.legend(['pay_checks','from_parents','venmo','other'])
    #for i in range(0,len(mo_dates)):
    #    num = int(round(bot[i]))
    #    text = '+$'+str(num)
    #    plt.text(i-0.4,bot[i]+100,text,fontsize=10)
    ##plt.title('Average from parents: \$'+str(av_fp)+'  Average from venmo: \$'+str(av_vnm))
    #plt.show()
    #im.savefig('expenses2019.jpg')

