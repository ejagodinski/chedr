#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 15 10:57:33 2020

@author: owner
"""
import sys
import os
import shutil
import csv
sys.path.insert(1, '/home/owner/codes/finance')
import finances_func as F
import datetime
from income_plot import inc_plot
from expense_plot import exp_plot
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import numpy as np
import pandas as pd
import glob
from fpdf import FPDF
dir_downloads = '/home/owner/Downloads/'
dir_finances = '/home/owner/Documents/finances/'
files = F.getfilesnew(dir_downloads,dir_finances)

#%%
class transaction:

    num_of_trans = 0

    def __init__(self, transact,day,month,year,descript,cat,subcat,card):
        self.transact = transact
        self.posneg = ['+' if transact > 0 else '-']
        self.day = day
        self.month = month
        self.year = year
        self.descript = descript
        self.cat = cat
        self.subcat = subcat
        self.card = card
        #self.sign = ['+' if self.posneg=='1' else '-'][0]

        transaction.num_of_trans += 1
        

    def printinfo(self):
        print(self.month+'/'+self.day+'/'+self.year)            
        print(self.transact)
        print(self.cat+'  '+str(self.subcat))
        result = ''.join([i for i in self.descript if not i.isdigit()])
        if '/' in result:
            result = result.split('/')[1]
        print(result+'\n')
#%% Placing new files
    def getfilesnew(dir_downloads,dir_finances):
        os.chdir(dir_downloads)
        recents = sorted(glob.glob('Checking*'))
        for file in recents:
            with open(file) as csv_file:
                csv_reader = csv.reader(csv_file,delimiter=',')
                for row in csv_reader:
                    m = row[0].split('/')[0]
                    y = row[0].split('/')[2]
            os.mkdir(dir_finances+y+'-'+m+'/')
            new_name = dir_finances+y+'-'+m+'/'+y+'_'+m+'.csv'
            old_name = dir_downloads+file
            shutil.copyfile(old_name,new_name)
#%% Selecting which csv to read
    def getfiles(dir_finances):
        os.chdir(dir_finances)
        dirs = sorted(glob.glob('*-*'))
        files = []
        for d in dirs:
            os.chdir(d)
            fil = glob.glob('*.csv')[0]
            files.append(dir_finances+d+'/'+fil)
            os.chdir('..')
        return files
#%% Reading the csv files 
    def read_files(files,card):
        csv_data = []
        for file in files:
            with open(file) as csv_file:
                csv_reader = csv.reader(csv_file,delimiter=',')
                for row in csv_reader:
                    date = row[0]
                    month = date.split('/')[0]
                    day = date.split('/')[1]
                    year = date.split('/')[2]
                    transact = float(row[1])
                    descript = row[4]
                    row_dict = transaction(transact,day,month,year,descript,'0ther',None,card)
                    csv_data.append(row_dict)

        return csv_data
#%% Discreminating between income and expenses
    def incexp(data):
        months,incomes,expenses = [],[],[]           
        # Add income and expenses
        for dat in data:
            mo = dat.month+'-'+dat.year
            if mo in months:
                if dat.transact > 0:
                    incomes[months.index(mo)] = incomes[months.index(mo)] + dat.transact
                else:
                    expenses[months.index(mo)] = expenses[months.index(mo)] + dat.transact
            else:
                months.append(mo)
                if dat.transact > 0:
                    incomes.append(dat.transact)
                    expenses.append(0)
                else:
                    expenses.append(dat.transact)
                    incomes.append(0)
        for ind in range(0,len(incomes)):
            incomes[ind]=round(incomes[ind],2)
            expenses[ind]=round(expenses[ind],2)
        return months,incomes,expenses
#%% Determining the balance from a starting point+incomes-expensese    
    def balance(starting,incomes,expenses):
        balances = [starting]
        for i in range(0,len(incomes)):
            new_bal = incomes[i]+expenses[i]+balances[i]
            balances.append(round(new_bal,2))
        return balances
#%% Determing the percent change monthly
    def endbaldiff(incomes,expenses,balances):
        diff = []
        for i in range(len(incomes)):
            d =round(incomes[i]+expenses[i],2)
            #pct.append(round(100*diff/balances[i],2))
            diff.append(d)
        return diff
#%% Plotting the monthly changes 
    def sixmonthlychanges(data,starting,dir_finances):
        # Add income and expenses
        months,incomes,expenses = transaction.incexp(data)
        bals = transaction.balance(starting,incomes,expenses)
        diff = transaction.endbaldiff(incomes,expenses,bals)
        bals6 = np.array(bals[-6:])
        diff6 = np.array(diff[-6:])
        y1 = abs(np.array(incomes[-6:]))
        y2 = abs(np.array(expenses[-6:]))
        y2_bot = bals6 - y2
        # Plot
        im, ax = plt.subplots(figsize=(6, 3))
        # Balance changes
        for i in range( 0, len(y1)):
            if diff6[i]>0:
                text = '+$'+str(diff6[i])
                txt_col = 'g'
            else:
                text = '-$'+str(abs(diff6[i]))
                txt_col = 'r'
            plt.text(i-0.5,bals6[i]+y1[i]+100,text,fontsize=10,color=txt_col,fontweight='bold')
        # Expenses
        ax.bar(months[-6:],y2,bottom = y2_bot,color='r')
        # Income
        ax.bar(months[-6:],y1,bottom=bals6[-6:],color='g')
        # Plot visuals
        end = bals6[-1] + incomes[-1] + expenses[-1]
        plt.ylabel('Balance')
        plt.title('Past 6 months of Income and Expenses\n'+'Started with \$'+str(bals[-6])+' and ended with \$'+str(round(end,2)))
        plt.ylim(-4000,8000)
        plt.xticks(rotation=45)
        im.tight_layout()
        plt.show()
        folder = dir_finances+'/'+months[-1].split('-')[1]+'-'+months[-1].split('-')[0]
        im.savefig(folder+'/'+months[-1]+'sixmo.jpg')
        return im
#%% Pie chart of current month expenses
    def monthpie(month,year,csv_data):
        mocats = []
        catdat = []
        for dat in csv_data:
            if dat.transact < 0:
                if dat.month==month:
                    if dat.year==year:
                        if dat.cat not in mocats:
                            mocats.append(dat.cat)
        for cat in mocats:
            locals()[cat] = 0
        for dat in csv_data:
            if dat.transact < 0:
                if dat.month==month:
                    if dat.year==year:
                        locals()[dat.cat] = locals()[dat.cat] + abs(dat.transact)
        for cat in mocats:
            catdat.append(round(locals()[cat],2))
        other = 0
        total = sum(catdat)
        othercat = []
        otherdat = []
        notcat = []
        notdat = []
        for dat in catdat:
            pct =100* dat/total
            index = catdat.index(dat)
            if pct < 5:
                other = other + dat
                othercat.append(mocats[index])
                otherdat.append(round(100*catdat[index]/total,2))
            else:
                notcat.append(mocats[index])
                notdat.append(catdat[index])
        notcat.append('other')
        other = round(other,2)
        notdat.append(other)
        fig1, ax1 = plt.subplots()
        ax1.pie(notdat,labels=notcat, autopct='%1.1f%%',shadow=True, startangle=60,labeldistance=1.1)
        plt.title('Transactions for '+month+'-'+year)
        plt.draw()
        plt.savefig('testpie.jpg')
#    return notcat,notdat,othercat,otherdat,total
#%% Saving the text
    def savetxt(csv_data,dir_finances):
        # Finding the latest month
        months,incomes,expenses = transaction.incexp(csv_data)
        folder = dir_finances+'/'+months[-1].split('-')[1]+'-'+months[-1].split('-')[0]
        im = folder+'/'+months[-1]+'sixmo.jpg'

        # Plotting the Model Performance
#        #         idFHund.plotRes(pred,y_test,H,out_dir,str(i).zfill(3)+'s_mse')
#        mpe = idFHund.plotRes(pred,y_test,H,out_dir,str(i).zfill(3)+'accloss')
#        
#        idFHund.savePlotDat(pred,y_test,H,mpe,out_dir,str(i).zfill(3))
#        
#        # Saving model
#        model.save(out_dir+'/'+str(i).zfill(3)+'model.h5')
#        model_json = model.to_json()
#        with open(out_dir+'/'+str(i).zfill(3)+'model.json','w') as json_file:
#            json_file.write(model_json)
#        
#        # Creating the PDF Report
        pdf = FPDF()
        pdf.image(im,
                  x = None, y = None, w = 150, h = 100, type = '', link = '')
#        pdf.add_page()
#        pdf.set_font('Arial', 'B', 9)
#        pdf.cell(0, 10, 'Output Report  Component: '+comp+'+'+str(shift)+'      Mean_pct_err: '+str("%.2f" % mpe),0,2,'L')
#        # Hyperparameters
#        pdf.cell(80, 5, "Batch Size: "+str(bs), 0, 0, 'L')
#        pdf.cell(0, 5, "Kernel Sizes: "+str(k1)+', '+str(k2)+', '+str(k3)+', '+str(k4), 0, 1, 'L')
#        pdf.cell(80, 5, "Epochs: "+str(e), 0, 0, 'L')
#        pdf.cell(0, 5, "Pooling Sizes: "+str(p1)+', '+str(p2)+', '+str(p3)+', '+str(p4), 0, 1, 'L')
#        pdf.cell(80, 5, "Drop Out: "+str(do), 0, 0, 'L')
#        pdf.cell(0, 5, "Weight Initialization: "+ki, 0, 1, 'L')
#        pdf.cell(80, 5, "Loss Function: "+l, 0, 0, 'L')
#        pdf.cell(0, 5, "Bias Initialization: "+bi, 0, 1, 'L')
#        pdf.cell(80, 5, "Optimization: "+str(opt).split()[0][18:], 0, 0, 'L')
#        pdf.cell(80, 5, "Learning Rate: "+str(lr), 0, 1, 'L')
#        pdf.cell(80, 5, "Decay: "+str(d), 0, 0, 'L')
#        pdf.cell(80, 5, "Momentum: "+str(m), 0, 1, 'L')
#        pdf.cell(80, 5, "Leaky Relu?: "+str(leak), 0, 0, 'L')
#        pdf.cell(80, 5, "Average Pooling?: "+str(avg), 0, 1, 'L')
#        
#        # Creating a table of NN layers overview for the report
#        stringlist=[]
#        model.summary(print_fn=lambda x: stringlist.append(x))
#        # Dataframe of the NN overview
#        df = pd.DataFrame()
#        layers=[];shape=[];param=[]
#        for n in range(3,len(stringlist)-5)[::2]:
#            ss = ''.join(stringlist[n].split(' '))
#            layers.append(ss.split('(')[0])
#            shape.append(ss.split('(')[2].split(')')[0])
#            param.append(ss.split(')')[2])
#        df['Layer'] = layers
#        df['Shape'] = shape
#        df['Parameters'] = param
#        # Adding NN overview DataFrame to the report
#        for n in range(0, len(df)):
#            pdf.cell(50, 6, '%s' % (df['Layer'].ix[n]), 1, 0, 'C')
#            pdf.cell(40, 6, '%s' % (df['Shape'].ix[n]), 1, 0, 'C')
#            pdf.cell(40, 6, '%s' % (df['Parameters'].ix[n]), 1, 2, 'C')
#            pdf.cell(-90)
#        pdf.cell(0, 5, stringlist[len(stringlist)-4], 0, 2, 'L')
#        
#        # Adding the Accuracy and Loss plot to the report
#        pdf.image(location+out_dir+'/'+str(i).zfill(3)+'accloss.png',
#                  x = None, y = None, w = 150, h = 100, type = '', link = '')
        
        #testParam = out_dir.replace('/','')
        
        # Outputting the report
        pdf.output('test.pdf', 'F')

    
    
    
    
    
    
    
#%% Categorizing===============================================================  
class category:
    num_of_cats = 0
    def __init__(self,descript,cat,subcat):
        self.descript = descript
        self.cat = cat
        self.subcat = subcat
        category.num_of_cats += 1

# For importing the categories.csv objects     
    def importCats(file):
        cat_data = []
        with open(file) as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=',')
            for row in csv_reader:
                descript = row[0]
                cat = row[1]
                try: 
                    row[2]
                    subcat = row[2]
                except:
                    subcat = None
                row_dict = category(descript,cat,subcat)
                cat_data.append(row_dict)
        return cat_data

# For categorizing    
    def define(csv_data,cat_data):
        cats = ['0ther']
        for dat in csv_data:
            for cat in cat_data:
                if cat.descript in dat.descript:
                    dat.cat = cat.cat
                    dat.subcat = cat.subcat
                    if cat.cat not in cats:
                        cats.append(cat.cat)
        return cats
                
# For anything needing more information for discerning                
    def redefine(csv_data,cat_data):
        for dat in csv_data:
            if "Wal-Mart" in dat.descript:
                if dat.transact > 50: # foods
                    dat.cat = 'food'; dat.subcat = 'walmart'
                else: # homes
                    dat.cat = 'purchases'; dat.subcat = 'walmart'
            elif "Microsoft" in dat.descript:
                if dat.transact > 60: # utilities
                    dat.cat = 'utilities'; dat.subcat = 'xbox live'
                else: # purchases
                    dat.cat = 'purchases'; dat.subcat = 'xbox'
            elif '0ther' in dat.cat:
                if ' FL ' not in dat.descript:
                    if 'PAYPAL' not in dat.descript:
                        if 'VENMO' not in dat.descript:
                            dat.subcat = 'travel'
                

    
#=============================================================================    
    
#%% Plotting transactions containing a 'string' within a selected year        
    def plot_string_year(data,string_in,year_in):
        months,trans = [],[]
        for dat in data:
            if year_in in dat.year:
                if string_in in dat.descript:
                    month_word = datetime.date(int(dat.year), int(dat.month), 1).strftime('%B')
                    if month_word not in months:
                        months.append(month_word)
                        trans.append(dat.transact)
                    else:
                        trans[months.index(month_word)] = trans[months.index(month_word)] + dat.transact
        
        im = plt.figure(num=None, figsize=(7, 3))
        plt.bar(months,trans)
        plt.xticks(rotation=45)
        plt.title(year_in+' by string: '+string_in)
        return im
        
#%% Tracking electric monthly    
    def track_elec(data):
        ct = 0
        dates = []
        datenum = []
        costs = []
        for dat in data:
            if '2019' in dat.year:
                if 'FPL' in dat.descript:
                    if ct == 0:
                        ct = ct + 1
                    else:
                        mo = dat.month
                        cost = dat.transact
                        if mo not in datenum:
                            month = datetime.date(int(dat.year), int(mo), 1).strftime('%B')
                            datenum.append(mo)
                        else:
                            prev = str(int(dat.month) - 1).zfill(2)
                            next0 = str(int(dat.month) + 1).zfill(2)
                            if prev in datenum:
                                mo = prev
                            else:
                                mo = next0
                            month = datetime.date(int(dat.year), int(mo), 1).strftime('%B')
                               
                        
                        dates.append(month)
                        costs.append(cost)
        im = plt.figure(num=None, figsize=(14, 7))
        plt.bar(dates,costs)
        plt.title(dat.year+' Yearly Electric')
        return im
