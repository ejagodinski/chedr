#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
sys.path.insert(1, '/home/owner/codes/finance/')
import finances_func as F
from income_plot import inc_plot
from expense_plot import exp_plot
from transaction import transaction
from transaction import category
import matplotlib.pyplot as plt
from fpdf import FPDF
import numpy as np

#% Get downloaded ones and get list of files
dir_downloads = '/home/owner/Downloads/'
dir_finances = '/home/owner/data/finances/'
transaction.getfilesnew(dir_downloads,dir_finances)
csv_data = transaction.read_files(transaction.getfiles(dir_finances),'debit')
#% Incomes and Expenses
starting = 1903.15 # Oct 1, 2019
#months,incomes,expenses = transaction.incexp(csv_data)
#balances = transaction.balance(starting,incomes,expenses)
#pct = transaction.endbalpct(incomes,expenses,balances)
im = transaction.sixmonthlychanges(csv_data,starting,dir_finances)
#transaction.plot_string_year(csv_data,'FPL','2019')
#% Importing cats from categories.csv
cat_data = category.importCats('/home/owner/data/finances/cats/categories.csv')
cats = category.define(csv_data,cat_data)
category.redefine(csv_data,cat_data)

#%%
cc_data = transaction.read_files(['/home/owner/data/finances/cc/creditcard.csv'],'cc')
for dat in cc_data:
    csv_data.append(dat)
#cats = category.define(cc_data,cat_data)
#category.redefine(cc_data,cat_data) 
#%%d
transaction.monthpie('03','2020',csv_data)  
#%%
def cats(month,year,csv_data):
    mocats = []
    catdat = []
    for dat in csv_data:
        if dat.transact < 0:
            if dat.month==month:
                if dat.year==year:
                    if dat.cat not in mocats:
                        mocats.append(dat.cat)
    for cat in mocats:
        locals()[cat] = 0
    for dat in csv_data:
        if dat.transact < 0:
            if dat.month==month:
                if dat.year==year:
                    locals()[dat.cat] = locals()[dat.cat] + abs(dat.transact)
    for cat in mocats:
        catdat.append(round(locals()[cat],2))
    return catdat,mocats

def income_cats(month,year,csv_data):
    mocats = []
    catdat = []
    for dat in csv_data:
        if dat.transact > 0:
            if dat.month==month:
                if dat.year==year:
                    if dat.cat not in mocats:
                        mocats.append(dat.cat)
    for cat in mocats:
        locals()[cat] = 0
    for dat in csv_data:
        if dat.transact > 0:
            if dat.month==month:
                if dat.year==year:
                    locals()[dat.cat] = locals()[dat.cat] + abs(dat.transact)
    for cat in mocats:
        catdat.append(round(locals()[cat],2))
    return catdat,mocats


# Finding the latest month
months,incomes,expenses = transaction.incexp(csv_data)
month = months[-1].split('-')[0]
year = months[-1].split('-')[1]
catdat,mocats = cats(months[-1].split('-')[0],months[-1].split('-')[1],csv_data)
incomedat,incomecat = income_cats(month,year,csv_data)
folder = dir_finances+months[-1].split('-')[1]+'-'+months[-1].split('-')[0]
im = folder+'/'+months[-1]+'sixmo.jpg'
balances = transaction.balance(starting,incomes,expenses)
final_bal = balances[-1]+incomes[-1]+expenses[-1]
catdat2,mocats2 = (list(t) for t in zip(*sorted(zip(catdat,mocats))))
catdat2.reverse(); mocats2.reverse()
#%%
# Creating the PDF Report
pdf = FPDF()
pdf.add_page()
pdf.set_font('Arial', 'B', 20)
pdf.cell(0, 10, 'Monthly Report: '+str(month)+'-'+str(year),0,2,'C')
pdf.set_font('Arial', 'B', 16)
pdf.cell(0, 10, 'Starting Bal: $'+str(balances[-1])+'      Ending Bal: $'+str(final_bal),0,2,'C')
pdf.set_font('Arial', 'B', 12)
pdf.cell(30, 8, '',0,2,'L')
pdf.cell(50, 8, 'Expenses',1,0,'C')
pdf.cell(60, 6, '',0,0,'C')
pdf.cell(50, 8, 'Incomes',1,1,'C')
#pdf.cell(-110)
pdf.cell(30, 8, 'Category',1,0,'L')
pdf.cell(20, 8, 'Amount',1,0,'L')
pdf.cell(60, 6, '',0,0,'C')
pdf.cell(30, 8, 'Category',1,0,'L')
pdf.cell(20, 8, 'Amount',1,1,'L')


for n in range(0, len(mocats2)):
    print(n)
    if n > len(incomedat)-1:
        pdf.cell(30, 6, mocats2[n], 1, 0, 'L')
        pdf.cell(20, 6, '$'+ "%0.2f"%catdat2[n], 1, 1, 'L')
    elif n == len(incomedat)-1:
        pdf.cell(30, 6, mocats2[n], 1, 0, 'L')
        pdf.cell(20, 6, '$'+ "%0.2f"%catdat2[n], 1, 0, 'L')
        pdf.cell(60, 6, '',0,0,'C')
        pdf.cell(30, 6, 'total: ', 0, 0, 'R')
        pdf.cell(20, 6, '$'+str(np.sum(incomedat)), 0, 1, 'L')
    else:
        pdf.cell(30, 6, mocats2[n], 1, 0, 'L')
        pdf.cell(20, 6, '$'+ "%0.2f"%catdat2[n], 1, 0, 'L')
        pdf.cell(60, 6, '',0,0,'C')
        pdf.cell(30, 6, incomecat[n], 1, 0, 'L')
        pdf.cell(20, 6, '$'+ "%0.2f"%incomedat[n], 1, 1, 'L')
pdf.cell(30, 6, 'total: ', 0, 0, 'R')
pdf.cell(20, 6, '$'+str(round(np.sum(catdat2),2)), 0, 1, 'L')        

imw = 130
imh = imw/2
pdf.image(im,x = 70, y = 90, w = imw, h = imh, type = '', link = '')
# Outputting the report
pdf.output(folder+'/test.pdf', 'F')


  
#%% Getting the months and years
mo_dates,mo = F.yrmo_dates(dir_finances,files)
# Fetch expenses and income
incomes,expenses = F.incexp(mo,mo_dates,files)

#%% Starting and Ending balances and Percent C
starting = 1903.15 # Oct 1, 2019
balances = F.balance(starting,incomes,expenses)
pct = F.endbalpct(incomes,expenses,balances)

#%% Make graphs of data
# List to array
F.monthlychanges(incomes,expenses,pct,balances,mo_dates,0,12)
#%% Create Income Plot
paychecks,from_parents = inc_plot(mo_dates[0:12],files[0:12])
#%%
exp_plot(mo_dates[0:12],files[0:12])